// Function for generating a random number between a min and max (inclusive).
export function randomInt( minInclusive, maxInclusive ) {
	return Math.floor(
		Math.random() * ( maxInclusive - minInclusive + 1 )
	) + minInclusive;
}

// Percent-encodes reserved URI characters so they can safely be used in a URI.
export function encodeCharsForURI( match ) {
	switch ( match ) {
		case ' ':
			return '%20';
		case '&':
			return '%26';
		case ',':
			return '%2C';
		case ';':
			return '%3B';
		case '?':
			return '%3F';
		default:
			return match;
	}
}

// Uses regex to find reserved URI characters in a string, and passes them to encodeCharsForURI.
export function encodeTextForURI( string ) {
	return string.replace( /\u0020|\u0026|\u002C|\u003B|\u003F/g, encodeCharsForURI );
}