/* global document */

// External dependencies
import React from 'react';
import ReactDOM from 'react-dom';

// Project component dependencies
import App from './components/App/index.jsx';

ReactDOM.render( <App />, document.getElementById( 'app-root' ) );