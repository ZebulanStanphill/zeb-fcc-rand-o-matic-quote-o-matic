// External dependencies
import React, { memo } from 'react';

// Internal dependencies
import './style.css';

export default memo( function Footer() {
	return (
		<footer className="zeb-footer">
			<p>© 2018-2019 <a href="https://zebulan.com" rel="external">Zebulan Stanphill</a></p>
			<p>Project released under <a rel="license external" href="https://www.gnu.org/licenses/gpl-3.0.html">GPLv3+</a></p>
			<p><a href="https://gitlab.com/ZebulanStanphill/zeb-fcc-rand-o-matic-quote-o-matic" rel="external">View source code</a></p>
		</footer>
	);
} );