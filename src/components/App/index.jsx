// External dependencies
import React from 'react';

// Project component dependencies
import Header from '../Header/index.jsx';
import Footer from '../Footer/index.jsx';
import RandomQuoteWidget from '../RandomQuoteWidget/index.jsx';

// Internal dependencies
import './style.css';
import quotes from './assets/quotes.json';

export default function App() {
	return (
		<div className="zeb-romqom">
			<Header />
			<RandomQuoteWidget quotes={ quotes } />
			<Footer />
		</div>
	);
}