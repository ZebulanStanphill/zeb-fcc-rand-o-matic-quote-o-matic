// External dependencies
import classNames from 'classnames';
import React, { useMemo, useState } from 'react';

// Project util dependencies
import { encodeTextForURI, randomInt } from '../../utils';

// Internal dependencies
import './style.css';

const themes = [
	'zeb-random-quote-widget--theme-1',
	'zeb-random-quote-widget--theme-2',
	'zeb-random-quote-widget--theme-3',
	'zeb-random-quote-widget--theme-4',
	'zeb-random-quote-widget--theme-5',
	'zeb-random-quote-widget--theme-6',
	'zeb-random-quote-widget--theme-7',
	'zeb-random-quote-widget--theme-8'
];

export default function RandomQuoteWidget( { quotes } ) {
	// I use functions to set some of the initial values here so that they are
	// not recalculated on every render.
	// https://reactjs.org/docs/hooks-faq.html#how-to-create-expensive-objects-lazily
	const [ quoteId, setQuoteId ] = useState( () => randomInt( 0, quotes.length - 1 ) );
	const quoteContent = useMemo( () => quotes[quoteId].content, [ quoteId ] );
	const quoteCitation = useMemo( () => quotes[quoteId].cite, [ quoteId ] );
	// Message used for tweet link.
	const tweetMessage = useMemo(
		() =>
			'“' +
			encodeTextForURI( quoteContent ) +
			'” — ' +
			encodeTextForURI( quoteCitation ),
		[ quoteId ]
	);
	const [ backgroundThemeClass, setBackgroundThemeClass ] = useState(
		() => themes[randomInt( 0, themes.length - 1 )]
	);

	function changeQuote() {
		/* Create an array of every quote except the one that is currently
		being used. Normally this means that the quotesExceptCurrent array
		will have one quote less than the quotes array, but this will not
		be the case if the currently-used quote is added by another script
		or has been manually inserted. In those cases, the
		quotesExceptCurrent array will be identical to the quotes array. */
		const quotesExceptCurrent = quotes.filter( ( quote, index ) => {
			return index !== quoteId ?
				true :
				false;
		} );

		/* If there is at least one quote to choose from that is not already in
		use, then choose a random quote that is not the current one. If there are
		no quotes to choose from (which happens when there is only one quote in
		the quotes array, then keep the current quote. */
		const newQuote =
			quotesExceptCurrent.length >= 1 ?
				quotesExceptCurrent[ randomInt( 0, quotesExceptCurrent.length - 1 ) ] :
				quotes[ quoteId ];

		// Set new quote id.
		// Quote ids are derived from the index of the quote in the quotes array.
		setQuoteId( quotes.indexOf( newQuote ) );
	}

	function changeTheme() {

		// Create an array of every theme except the one that is already in use.
		const themesExceptCurrent = themes.filter(
			theme => theme !== backgroundThemeClass
		);

		/* If there is at least one theme to choose from that is not already in
		use, then choose a random theme that is not the current one. If there no
		themes to choose from (which happens when there is only one theme in the
		themes array or multiple theme classes are added by some other script or
		something), then choose any random theme from the themes array. */
		const newTheme = themesExceptCurrent.length >= 1 ?
			themesExceptCurrent[randomInt( 0, themesExceptCurrent.length - 1 )] :
			themes[randomInt( 0, themes.length - 1 )];

		// Set the new theme.
		setBackgroundThemeClass( newTheme );
	}

	function changeQuoteAndTheme() {
		changeQuote();
		changeTheme();
	}

	return (
		<div className={ classNames( 'zeb-random-quote-widget', backgroundThemeClass ) }>
			<blockquote className="zeb-random-quote-widget__blockquote">
				<p>{ quoteContent }</p>
				<footer>
					<cite className="zeb-random-quote-widget__citation">{ quoteCitation }</cite>
				</footer>
			</blockquote>
			<div className="zeb-random-quote-widget__action-container">
				<button
					className="zeb-random-quote-widget__button"
					type="button"
					onClick={ changeQuoteAndTheme }
				>
					Change Quote
				</button>
				<a
					className="zeb-random-quote-widget__tweet-quote-link"
					href={ 'https://twitter.com/intent/tweet?text=' + tweetMessage }
					rel="external"
				>
					Tweet Quote
				</a>
			</div>
		</div>
	);
}