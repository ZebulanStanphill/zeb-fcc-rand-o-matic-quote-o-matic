// External dependencies
import React, { memo } from 'react';

// Internal dependencies
import './style.css';

export default memo( function Header() {
	return (
		<header className="zeb-romqom-header">
			<h1 className="zeb-romqom-header__h1">Zeb’s<wbr /> <span className="zeb-romqom-header__no-wrap">Rand-o-Matic</span><wbr /> <span className="zeb-romqom-header__no-wrap">Quote-o-Matic</span></h1>
		</header>
	);
} );