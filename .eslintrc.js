module.exports = {
	root: true,
	env: {
		es6: true
	},
	extends: [
		'eslint:recommended',
		'plugin:jsx-a11y/recommended',
		'plugin:react/recommended'
	],
	parserOptions: {
		ecmaFeatures: {
			jsx: true
		},
		ecmaVersion: 2018,
		sourceType: 'module'
	},
	plugins: [
		'jsx-a11y',
		'react',
		'react-hooks'
	],
	rules: {
		'indent': [
			'error',
			'tab',
			{ "SwitchCase": 1 }
		],
		'linebreak-style': [
			'error',
			'unix'
		],
		'quotes': [
			'error',
			'single'
		],
		'semi': [
			'error',
			'always'
		],
		'jsx-a11y/media-has-caption': 'off',
		'react/destructuring-assignment': 'warn',
		'react/display-name': 'off',
		'react/jsx-uses-react': 'error',
		'react/no-access-state-in-setstate': 'error',
		'react/no-danger-with-children': 'error',
		'react/no-did-mount-set-state': 'error',
		'react/no-did-update-set-state': 'error',
		'react/no-redundant-should-component-update': 'error',
		'react/no-this-in-sfc': 'error',
		'react/no-unused-state': 'error',
		'react/no-will-update-set-state': 'error',
		'react/prefer-es6-class': 'error',
		'react/prefer-stateless-function': 'error',
		'react/prop-types': 'off',
		'react/jsx-filename-extension': [
			'warn',
			{ 'extensions': [ '.jsx' ] }
		],
		'react-hooks/rules-of-hooks': 'error'
	},
	settings: {
		'react': {
			'pragma': 'React',
			'version': '16.8'
		}
	}
};