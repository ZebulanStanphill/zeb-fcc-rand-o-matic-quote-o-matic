# Zeb’s Rand-o-Matic Quote-o-Matic

## Description
A random quote displayer built using React for a freeCodeCamp project.

## Usage
1. Open a terminal at the project root.
2. Run `npm install` to install the package dependencies.
3. Run `npm run build` to build the project.
4. Open `dist/index.html` to view/use the project.

## Licensing info

© 2018-2019 Zebulan Stanphill

This program is free (as in freedom) software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see https://www.gnu.org/licenses/.

This project uses the Nunito font:

https://github.com/vernnobile/NunitoFont

© 2014 Vernon Adams (vern@newtypography.co.uk).

Released under the SIL Open Font License, Version 1.1. See `nunito-license.txt`.